#!/usr/bin/python3
# -*- coding: utf-8  -*-

'''
atx400ColorEditor.

Usage:
  param [options]

Options:
'''

import flask
from flask import request
import time
import docopt
import json
import os.path
import os
from os.path import exists
import sys
import logging
import socketserver
import socket
import wsgiref.simple_server
import threading

from version import __version__
import atxpylib.utils as atxutils
from atxpylib.quitfile import QuitFile


# BEGIN ######################### copy Radek code from web.py ####################################### TODO
class MyServer(socketserver.ThreadingMixIn, wsgiref.simple_server.WSGIServer):
    address_family = socket.AF_INET6  # TODO: hack until this is fixed in python itself
    timeout = 1

    def serve_forever(self):
        self._do_serve = 1
        while self._do_serve:
            self.handle_request()


class MyHandler(wsgiref.simple_server.WSGIRequestHandler):
    def handle(self):
        try:
            return super().handle()
        except:
            # this happens on client disconnect
            logging.info('silencing broken pipe error')
# END ######################### copy Radek code from web.py ####################################### TODO

basedir = os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__)
app = flask.Flask(__name__, template_folder=os.path.join(basedir, 'templates'), static_folder=os.path.join(basedir, 'static'))


@app.route('/', methods=['GET', 'POST'])
def index():
    data = request.form.to_dict(flat=False)
    themeName = request.args.get('theme')
    if themeName:
        themeFn = themeName
        theme = "theme=%s" % themeName
        fn_forSave = themeName
    else:
        themeFn = "default"
        theme = ""
        fn_forSave = "custom"
    if exists("../pycontrol/static/default.css"):
        path = "../pycontrol/static"
    elif exists("../control/static/default.css"):
        path = "../control/static"
    else:
        path = "./"
    logging.info("using path: %s" % path)
    if not data:
        data = {}
    else:
        fn = ""
        lines = []
        lines.append(":root {\n")
        for k,v in data.items():
            if k != 'file':
                lines.append(k + ": " + v[0] + ";\n")
            else:
                fn = v[0]
        lines.append("}\n")
        if fn != "":
            f = open("%s/%s.css" % (path, fn), "w")
            f.writelines(lines)
            f.close()
            theme = "theme=%s" % fn
            themeFn = fn
    html = """
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="/static/style.css">
    <script src='./static/jscolor.js'></script>
    <script src='./static/script.js'></script>
  </head>
  <body>
    <iframe id="control" src="http://localhost:7778/?%s" width="1280" height="768"></iframe>
    <form method="POST">
""" % theme
    if exists("%s/%s.css" % (path, themeFn)) :
        f = open("%s/%s.css" % (path, themeFn), "r");
        logging.info('opening theme file %s/%s.css' % (path, themeFn))
        for line in f.readlines():
            if not '--' in line:
                continue
            sline = line.split(":")
            name = sline[0]
            val = sline[1].replace(";","")
            html += '<div class="line"><input name="%s" value="%s" data-jscolor="">%s</div>' % (name, val, name)
        f.close()
    html += """
      <input type="text" name="file" value="%s" placeholder="name of file like 'custom'">
      <input type="submit" value="Save">
    </form>
  </body>
</html>
""" % fn_forSave
    return html


global thr_run
thr_run = True  # TODO: ugly shit


def thr():
    logging.debug('starting quit thread loop')
    while thr_run:
        # BEGIN ############################ copy Quit Radek function with change info message and stop() function #################################################
        quit_fn = 'quit'
        qf = QuitFile(quit_fn)
        if qf.check_and_delete():
            logging.info('quit file found, exiting')
            stop()
        # END ############################ copy Quit Radek function with change info message and stop() function #################################################
        time.sleep(1)  # TODO: hard-coded shit
    logging.debug('stopped quit thread loop')


def stop():
    global srv
    if srv:
        srv._do_serve = 0


def main():
    args = docopt.docopt(__doc__, version=__version__)
    atxutils.logging_setup('DEBUG', '../log/atx400ColorEditor.log')
    logging.info('*' * 40)
    logging.info('starting atx400ColorEditor v%s' % __version__)
    logging.debug('args: %s' % dict(args))

    thr_q = threading.Thread(target=thr, daemon=True)
    thr_q.start()

    quit_fn = 'quit'
    qf = QuitFile(quit_fn)
    if qf.check_and_delete():
        logging.info('stale quit file found, deleted')

    web_run()


def web_run():
    global srv
    port = 7709
    srv = wsgiref.simple_server.make_server('', port, app, MyServer, MyHandler)
    logging.info('starting server, port: %s' % port)
    srv.serve_forever()
    logging.debug('server stopped')
    logging.debug('joining quit thread')
    srv.server_close()
    srv = None
    logging.debug('web exit')


if __name__ == '__main__':
    sys.exit(main())